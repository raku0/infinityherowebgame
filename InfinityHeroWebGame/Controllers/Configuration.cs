﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InfinityHeroWebGame.Domain.Entities;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using Ninject;
using InfinityHeroWebGame.Infrastructure.Interface;
using InfinityHeroWebGame.Domain.InfinityHeroDBContex;
using InfinityHeroWebGame.Domain.Constans;


namespace InfinityHeroWebGame.Models
{
    public class Configuration :IConfiguration
    {
        IKernel ninja = new StandardKernel();
        InfinityHeroContext dbc;
        string pathToConfiguration = HttpContext.Current.Server.MapPath("\\App_Data\\Configuration");
        string pathToLog = HttpContext.Current.Server.MapPath("\\App_Data\\ServerLog.txt");

        public Configuration()
        {           
            dbc = ninja.Get<InfinityHeroContext>();
            CreateLogFileIfNotExist();
            if (!IsExistConfiguration())
                CreateConfiguration();
            else
                ReadAndSetExistingCondifuration();
            dbc.AddMessageToLog("Application starting..");
        }
        //After add a new attribute add it here and in enums.ConfigurationAttributes
        public string CreateConfiguration()
        {
            if (IsExistConfiguration())
                return "Configuration allready exist. ";
            try
            {              
                byte[] newLine = Encoding.ASCII.GetBytes(Environment.NewLine);
                byte[] needLogin = new UTF8Encoding(true).GetBytes("NeedLogin:true");
                byte[] expRate = new UTF8Encoding(true).GetBytes("ExpRate:1,0");
                byte[] gameSpeed = new UTF8Encoding(true).GetBytes("GameSpeed:1,0");
                byte[] goldRate = new UTF8Encoding(true).GetBytes("GoldRate:1,0");
                //If u wanna to have item drop as some lvl, Bloody glove 1-15 lvl. Magic sword 20-25 etc change it to false
                byte[] itemDropGraphicsRandom = new UTF8Encoding(true).GetBytes("ItemDropGraphicsRandom:true");
                byte[] doubleDrop = new UTF8Encoding(true).GetBytes("DoubleDrop:false");
                using (FileStream fs = File.Open(pathToConfiguration,FileMode.OpenOrCreate)) { 
                    fs.Write(needLogin, 0, needLogin.Length);
                    fs.Write(newLine, 0, newLine.Length);
                    fs.Write(expRate, 0, expRate.Length);
                    fs.Write(newLine, 0, newLine.Length);
                    fs.Write(gameSpeed, 0, gameSpeed.Length);
                    fs.Write(newLine, 0, newLine.Length);
                    fs.Write(goldRate, 0, goldRate.Length);
                    fs.Write(newLine, 0, newLine.Length);                   
                    fs.Write(itemDropGraphicsRandom, 0, itemDropGraphicsRandom.Length);
                    fs.Write(newLine, 0, newLine.Length);
                    fs.Write(doubleDrop, 0, doubleDrop.Length);                   
                    fs.Close();
            }
                ConfigurationEntity conf = new ConfigurationEntity();               
                conf.ExpRate = 1;
                conf.NeedLogin = false;
                conf.GameSpeed = 1;
                conf.GoldRate = 1;
                conf.GenerateItemRandom = true;
                conf.DoubleDrop = false;
                dbc.ConfigurationGame.Add(conf);
                dbc.SaveChanges();               
                return "Configuration created. ";
            }
            catch(Exception e)
            {
                return "Problem with creating configuration. "+e.Message;
            }
        }
        public List<string> GetAttributes()
        {           
            List<string> attributes=new List<string>();      
            if (!IsExistConfiguration())
                CreateConfiguration();
            using (StreamReader sr = new StreamReader(pathToConfiguration))
            {
                while(sr.Peek()>=0)
                {
                    Match m = Regex.Match(sr.ReadLine(), @"[^:]*");
                    attributes.Add(m.Value);
                }
                sr.Close();
            }
                return attributes;
        }
        public Dictionary<string, string> GetConfiguration()
        {
            Dictionary<string, string> configuration = new Dictionary<string, string>();
            if (!IsExistConfiguration())
                CreateConfiguration();
            Regex regex = new Regex(@"[^:]*");
            Regex regex2 = new Regex(@"(?<=:)[\w+.-]+");
            using (StreamReader sr = new StreamReader(pathToConfiguration))
            {
                while (sr.Peek() >= 0)
                {
                    string line = sr.ReadLine();
                    Match key = regex.Match(line);
                    Match value= regex2.Match(line);
                    configuration.Add(key.Value, value.Value);
                }
                sr.Close();
            }
            return configuration;
        }
        public string GetValueFromAttribute(Enums.ConfigurationAttributes attribute)
        {
            //\bObjectName:\K\S+            
            using(StreamReader sr=new StreamReader(pathToConfiguration))
            {
                string line;
                while ((line=sr.ReadLine())!=null)
                {
                    Regex regex = new Regex(@"\b" + attribute.ToString() + @":(\S+)");
                    Match m = regex.Match(line);
                    if (m.Success)
                        return m.Groups[1].Value;                   
                }
                sr.Close();
            }
            return "Error";
        }
        public bool IsExistConfiguration()
        {
            return File.Exists(pathToConfiguration);
        }
        public bool RestoreDefault()
        {
            if (File.Exists(pathToConfiguration))
                File.Delete(pathToConfiguration);
            try
            {
                CreateConfiguration();
                return true;
            }
            catch(Exception e)
            {
                dbc.AddMessageToLog("Problem with restore default configuration. "+e.Message);
                return false;
            }
        }
        public bool ReadAndSetExistingCondifuration()
        {
            try
            {
                ConfigurationEntity configurationEntity = dbc.ConfigurationGame.Single();
                configurationEntity.ExpRate = double.Parse(GetValueFromAttribute(Enums.ConfigurationAttributes.ExpRate));
                configurationEntity.NeedLogin = bool.Parse(GetValueFromAttribute(Enums.ConfigurationAttributes.NeedLogin));
                dbc.SaveChanges();
                dbc.AddMessageToLog("Configuration loaded.");
                return true;
            }
            catch (Exception e)
            {
                dbc.AddMessageToLog("Problem with load existing configuration. "+e.Message);
                return false;
            }
        }
        public bool CreateLogFileIfNotExist()
        {
            if (!File.Exists(pathToLog))
            {
                try
                {
                    FileStream logFile = File.OpenWrite(pathToLog);
                    logFile.Close();
                    dbc.AddMessageToLog("Log created. ");
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }
        public void ChangeAttributes(Enums.ConfigurationAttributes attributes)
        {
            throw new NotImplementedException();
        }
    }
}