﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web;
using System.Data.Entity;
using InfinityHeroWebGame.Domain.Entities;
using Ninject;
using InfinityHeroWebGame.Domain.InfinityHeroDBContex;
using InfinityHeroWebGame.Domain.Constans;

namespace InfinityHeroWebGame.Controllers
{
    public class HomeController : Controller
    {
        IKernel ninja = new StandardKernel();
        readonly string GraphicsPath = "../Content/items/";
        AccountEntity ac;
        // GET: Index
        public ActionResult Index()
        {
            InfinityHeroContext db = ninja.Get<InfinityHeroContext>();
            if (db.IsNeedLogin()&&Session["Account"]==null||Session.IsNewSession&&Session["Account"] == null)
            {               
                return RedirectToAction("Login");
            }
            //Need be here because is possible to not login
            ac = (AccountEntity)Session["Account"];
            if (ac.Character == null)
            {
                return RedirectToAction("CreateCharacter");
            }
            if ( ac.IsBanned)
            {
                return RedirectToAction("Banned");
            }
            LoadItems();
            return View(Session["Account"]);
        }
        public ActionResult Banned()
        {
            CheckSessionIsExpired();
            return View();
        }
        public ActionResult Logout()
        {
            Session["Account"] = null;
            return RedirectToAction("Index");
        }
        //GET: Create Character
        public ActionResult CreateCharacter()
        {
            CheckSessionIsExpired();
            return View();
        }
        [HttpPost]
        public ActionResult CreateCharacter(CharacterEntity character)
        {
            CheckSessionIsExpired();
            if (character.Name==null||character.Name.Trim()==""||character.Name.Length<4)
            {
                ViewBag.Error = "Name cannot be empty and less than 4";
                return View();
            }
            int total = character.Strength + character.Rambler + character.Intelligence + character.Vitality + character.Agility+character.Luck;
            if (total > 50 || character.Strength<5||character.Rambler<5||character.Vitality<5||character.Luck<5||character.Intelligence<5||character.Agility<5)
            {
                ViewBag.Error = "Nice try ¯\\_(ツ)_/¯";
                return View();
            }
            if (total < 50)
            {
                ViewBag.Error = "Use all of your point.";
                return View();
            }
            ac = (AccountEntity)Session["Account"];
            InfinityHeroContext db = ninja.Get<InfinityHeroContext>();
            AccountEntity acc = db.GameAccount.Find(ac.Id);
            character.CurrentHp = character.MaxHp;
            character.CurrentMp = character.MaxMp;
            acc.Character = character;           
            db.SaveChanges();
            acc.Character.AddMessageToLog("Character created.");
            ItemEntity item = new ItemEntity
            {
                Name = "Young Adventure Sword",
                IsEquipment = true,
                IsDoubleSlot = false,
                Attack = 5,
                ItemGraphics = "pirate_sword.png",
                Type = Enums.ItemType.Weapon               
            };
            acc.Character.Items.Add(item);
            db.SaveChanges();
            Session["Account"] = acc;
            return RedirectToAction("Index");
        }

        //Get: Login
        public ActionResult Login()
        {
            if(Session["Account"] == null)
                return View();
            return RedirectToAction("Index");
        }
        //Post: Login to Account
        [HttpPost]
        public ActionResult Login(AccountEntity account)
        {
            InfinityHeroContext db = ninja.Get<InfinityHeroContext>();
            if (db.GameAccount.SingleOrDefault(acc => acc.Login == account.Login) != null && db.VerifyUserPassword(account.Password, account.Login))
            {
                Session["Account"]=db.GameAccount.Single(acc => acc.Login == account.Login);
                ac = (AccountEntity)Session["Account"];             
                if (ac.Character!=null)
                {
                    string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (string.IsNullOrEmpty(ip))
                    {
                        ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                    }
                    ac.Character.AddMessageToLog("Login from: " + ip);
                }               
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.LoginError = "Login or password incorrect. ";
                return View();
            }
        }
        //MainGame Partial View
        public PartialViewResult CharacterView()
        {
            CheckSessionIsExpired();
            LoadItems();
            return PartialView("CharacterPV", Session["Account"]);
        }
        public PartialViewResult TrainingView()
        {
            CheckSessionIsExpired();
            return PartialView("TrainingPV");
        }
        public ActionResult Register()
        {
            if (Session["Account"] == null)
                return View();
            return RedirectToAction("Index");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(AccountEntity account)
        {
            InfinityHeroContext db = ninja.Get<InfinityHeroContext>();
            var dbLogin = db.GameAccount.SingleOrDefault(acc => acc.Login == account.Login);
            if (ModelState.IsValid && dbLogin==null)
            {
                account.Password = db.HashPassword(account.Password);
                db.GameAccount.Add(account);
                db.SaveChanges();
                db.AddMessageToLog("Account: " + account.Login + " created.");
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.LoginDuplicate = "Login allready taken pick another. ";
                return View();
            }
        }
        public void LoadItems()
        {
            ac = (AccountEntity)Session["Account"];
            if (ac.Character.Items.Count > 0)
            {
                foreach (var item in ac.Character.Items)
                {
                    if (item.IsEquipment && item.Type == Enums.ItemType.Weapon)
                        ViewData["Weapon"] = GraphicsPath + item.ItemGraphics;
                    if (item.IsEquipment && item.Type == Enums.ItemType.Shield)
                        ViewData["Shield"] = GraphicsPath + item.ItemGraphics;
                    if (item.IsEquipment && item.Type == Enums.ItemType.Armor)
                        ViewData["Armor"] = GraphicsPath + item.ItemGraphics;
                    if (item.IsEquipment && item.Type == Enums.ItemType.Boots)
                        ViewData["Boots"] = GraphicsPath + item.ItemGraphics;
                    if (item.IsEquipment && item.Type == Enums.ItemType.Earrings)
                        ViewData["Earrings"] = GraphicsPath + item.ItemGraphics;
                    if (item.IsEquipment && item.Type == Enums.ItemType.Glove)
                        ViewData["Glove"] = GraphicsPath + item.ItemGraphics;
                    if (item.IsEquipment && item.Type == Enums.ItemType.Helmet)
                        ViewData["Helmet"] = GraphicsPath + item.ItemGraphics;
                    if (item.IsEquipment && item.Type == Enums.ItemType.Necklace)
                        ViewData["Necklace"] = GraphicsPath + item.ItemGraphics;
                    if (item.IsEquipment && item.Type == Enums.ItemType.Ring)
                        ViewData["Ring"] = GraphicsPath + item.ItemGraphics;
                }
            }
        }
        public void CheckSessionIsExpired()
        {
            if (Session.IsNewSession)
                RedirectToAction("Login");
        }
    }
}