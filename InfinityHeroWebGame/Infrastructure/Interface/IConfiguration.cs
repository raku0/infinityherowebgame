﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfinityHeroWebGame.Domain.Constans;

namespace InfinityHeroWebGame.Infrastructure.Interface
{
    public interface IConfiguration
    {
        Dictionary<string,string> GetConfiguration();
        List<string> GetAttributes();
        string GetValueFromAttribute(Enums.ConfigurationAttributes attribute);
        bool IsExistConfiguration();
        string CreateConfiguration();
        bool RestoreDefault();
        void ChangeAttributes(Enums.ConfigurationAttributes attributes);
    }
}
