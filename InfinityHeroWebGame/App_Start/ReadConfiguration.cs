﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using InfinityHeroWebGame.Models;
using InfinityHeroWebGame.Domain.InfinityHeroDBContex;
using InfinityHeroWebGame.Infrastructure.Interface;
using Ninject;

namespace InfinityHeroWebGame.App_Start
{
    public class ReadConfiguration
    {
        public static void Init()
        {
            IKernel ninja = new StandardKernel();
            ninja.Bind<IConfiguration>().To<Configuration>().InSingletonScope();
            ninja.Bind<InfinityHeroContext>().To<InfinityHeroContext>().InSingletonScope();
            IConfiguration conf = ninja.Get<IConfiguration>();
           
        }
    }
}