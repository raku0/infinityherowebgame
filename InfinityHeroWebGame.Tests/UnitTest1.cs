﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using InfinityHeroWebGame.Models;
using System.Collections.Generic;
using InfinityHeroWebGame.Infrastructure.Interface;

namespace InfinityHeroWebGame.Tests
{
    [TestClass]
    public class UnitTest
    {
        private IConfiguration getTestConfiguration()
        {
            return new Configuration();
        }
        
        [TestMethod]
        public void Test1()
        {
            IConfiguration conf = getTestConfiguration();
            List<string> list = new List<string>();
            list.AddRange(Enum.GetNames(typeof(Enums.ConfigurationAttributes)));
            Assert.AreEqual(conf.GetAttributes(), list);

        }
    }
}
