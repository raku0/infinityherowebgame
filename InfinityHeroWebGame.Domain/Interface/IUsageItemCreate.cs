﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfinityHeroWebGame.Domain.Entities;
using InfinityHeroWebGame.Domain.Parent;
using InfinityHeroWebGame.Domain.Constans;

namespace InfinityHeroWebGame.Domain.Interface
{
    public interface IUsageItemCreate
    {
        ItemEntity CreateRandomItem(Creature creatureToAddItem);
        ItemEntity CreateItem(Creature creatureToAddItem, Enums.ItemType itemType);
    }
}
