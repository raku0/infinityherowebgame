﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfinityHeroWebGame.Domain.Entities;
using InfinityHeroWebGame.Domain.Parent;

namespace InfinityHeroWebGame.Domain.Interface
{
    interface IConsumableItemCreate
    {
        ConsumableEntity CreateRandomConsumableItem(Creature creatureToAddItem);
    }
}
