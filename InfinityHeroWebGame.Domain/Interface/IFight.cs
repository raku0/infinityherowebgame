﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfinityHeroWebGame.Domain.Entities;
using InfinityHeroWebGame.Domain.Parent;

namespace InfinityHeroWebGame.Domain.Interface
{
    public interface IFight
    {
        //Return true if is dead
        void Attack(Creature Attacker,Creature Defender);
        //TODO spells
        //void UseSpell(Person CharacterOrMonster, Spells spell);
        bool IsDeath(Creature creature);
    }
}
