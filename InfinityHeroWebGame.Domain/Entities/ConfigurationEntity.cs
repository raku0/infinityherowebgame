﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace InfinityHeroWebGame.Domain.Entities
{
    public class ConfigurationEntity
    {
        [Key]
        public int Id { get; set; }
        public bool NeedLogin { get; set; }
        public double ExpRate { get; set; }
        public double GameSpeed { get; set; }
        public double GoldRate { get; set; }
        public bool GenerateItemRandom { get; set; }
        public bool DoubleDrop { get; set; }
    }
}