﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using InfinityHeroWebGame.Domain.Interface;
using InfinityHeroWebGame.Domain.Constans;
using InfinityHeroWebGame.Domain.Parent;



namespace InfinityHeroWebGame.Domain.Entities
{
    public class CharacterEntity : Creature
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MinLength(4, ErrorMessage = "Min lenght is 4.")]
        [MaxLength(20, ErrorMessage = "Max lenght is 20.")]
        public string Gender { get; set; }
        public int Exp { get; private set; }
        public int Gold { get; private set; }
        //PREMIUM 
        public int HeroCoint { get; set; }
        //Dmg
        public int Strength { get; set; }
        //Max mp and magic damage
        public int Intelligence { get; set; }
        //% to hit
        public int Agility { get; set; }
        //Crit
        public int Luck { get; set; }
        //Rare item find +      
        public int Rambler { get; set; }
        //Max hp
        public int Vitality { get; set; }
        public int StatsLeft { get; private set; }
        //Profesion
        public Enums.HeroClass? HeroClass { get; set; }
        //SubProfesion
        public Enums.SubClass? SecondClass { get; set; }
        //Destiny
        public Enums.Destiny? DestinyHero { get; set; }
        public new int MaxHp => Lvl * 100 + (Vitality * 10);
        public new int MaxMp => Lvl * 20 + (Intelligence * 5);
        public int Honor { get; set; }
        public int BonusItemFind
        {
            get
            {
                int ItemFind =(int)Math.Round((double)(Rambler / Lvl),1);
                if (Items.Count > 0)
                {
                    foreach (var item in Items)
                    {
                        if (item.IsEquipment && item.BonusValue.TryGetValue(Enums.BonusType.AddRambler, out int Bonus))
                            ItemFind += Bonus;
                    }
                }
                return ItemFind > 20 ? 20 : ItemFind;
            }
         }
        public int ExpForNextLvl
        {
            get
            {
                return Lvl * 1000;
            }
        }
        public int MaxMoves { get; set; }
        public int CurrentMoves { get;set; }
        public int Kills { get; set; }
        public int Deaths { get; set; }
        public new double MinMagickAttack {
            get
            {
                int MagAttack = 5+Intelligence+3;
                if (Items.Count > 0)
                {
                    foreach (var item in Items)
                    {
                        if (item.IsEquipment && item.MagicAttack.HasValue)
                            MagAttack += (int)item.MagicAttack;
                    }
                }
                return MagAttack * 0.8;

            }
        }
        public new double MaxMagickAttack
        {
            get
            {
                int MagAttack = 5 + Intelligence + 3;
                if (Items.Count > 0)
                {
                    foreach (var item in Items)
                    {
                        if (item.IsEquipment && item.MagicAttack.HasValue)
                            MagAttack += (int)item.MagicAttack;
                    }
                }
                return MagAttack * 1.2;

            }
        }
        public new int Defense
        {
            get
            {
                int Defense = 0;
                if (Items.Count > 0)
                {
                    foreach (var item in Items)
                    {
                        if (item.IsEquipment && item.Defense.HasValue)
                            Defense += (int)item.Defense;
                    }
                }
                return Defense;
            }
        }
        public new int MagicDefense
        {
            get
            {
                int MagicDefense = 0;
                if (Items.Count > 0)
                {
                    foreach (var item in Items)
                    {
                        if (item.IsEquipment && item.MagicDefense.HasValue)
                            MagicDefense += (int)item.MagicDefense;
                    }
                }
                return MagicDefense;
            }
        }
        public new int HitRate {
            get
            {
                int HitPercent = 50+(int)Math.Round((double)(Agility/Lvl));
                if (Items.Count>0)
                {
                    foreach (var item in Items)
                    {
                        if (item.IsEquipment && item.BonusValue.TryGetValue(Enums.BonusType.AddHitPercent, out int Bonus))
                            HitPercent += Bonus;
                    }
                }
                return HitPercent;
            } }
        public new int CritRate {
            get
            {
                int CritPercent = 5 + Luck;
                if (Items != null)
                {
                    foreach (var item in Items)
                    {
                        if (item.IsEquipment && item.BonusValue.TryGetValue(Enums.BonusType.AddCritPercent, out int Bonus))
                            CritPercent += Bonus;
                    }
                }
                return CritPercent>90?90:CritPercent;
            }
        }
        public new double MinAttack
        {
            get
            {
                int MinAttack=Strength*3;
                if (Items.Count > 0)
                {
                    foreach (var item in Items)
                    {
                        if (item.IsEquipment && item.Attack.HasValue)
                            MinAttack += (int)item.Attack;
                    }
                }
                return MinAttack*0.8;
            }            
        }
        public new double MaxAttack
        {
            get
            {
                int totalAttack = Strength*3;
                if (Items.Count > 0)
                {
                    foreach (var item in Items)
                    {
                        if (item.IsEquipment && item.Attack.HasValue)
                            totalAttack += (int)item.Attack;
                    }
                }
                return totalAttack*1.2;
            }
        }
        private void AddExp(Creature enemy)
        {
            if (IsDeath(enemy))
            {
                int ActualExp = Exp + GetExp(enemy);
                if (ExpForNextLvl > ActualExp)
                {
                    Exp = ActualExp;
                }
                else
                {
                    Exp = ActualExp - (Lvl * 1000);
                    AddLvlAndStats();
                }
            }
        }
        private void AddGold(Creature enemy)
        {
            if (IsDeath(enemy))
            {
                Gold += GetGold(enemy);
            }
        }
        private void AddDrop(MonsterEntity enemy)
        {
            if (IsDeath(enemy)&&enemy.ConsumableItemFromKill.Count>0)
            {
                foreach (ItemEntity item in enemy.ItemFromKill)
                {
                    Items.Add(item);
                    AddMessageToLog("Received item: "+item.Name);
                }
                
            }
            if (IsDeath(enemy) && enemy.ConsumableItemFromKill.Count > 0)
            {
                foreach (ConsumableEntity item in enemy.ConsumableItemFromKill)
                {
                    ConsumableItems.Add(item);
                    AddMessageToLog("Received consumable item: " + item.Name);
                }
            }
        }
        //For other players
        private void GetAllDrop(Creature enemy)
        {
            AddExp(enemy);
            AddGold(enemy);
        }
        //For monster
        private void GetAllDrop(Creature enemy,MonsterEntity monster)
        {
            AddExp(enemy);
            AddGold(enemy);
            AddDrop(monster);
            AddMessageToLog("Received drop from "+monster.Name);
        }
        private void AddLvlAndStats()
        {
            Lvl += 1;
            StatsLeft += 3;
            HeroCoint += 5;
            AddMessageToLog("Hero lvl up! Actual "+Lvl+"lvl");

        }
        public virtual ICollection<ItemEntity> Items { get; set; }
        public virtual ICollection<ConsumableEntity> ConsumableItems { get; set; }
        private bool IsSpaceInBp(Creature enemy)
        {
            int[,] VirtualBp = new int[9, 9];
            try
            {
                foreach (var item in Items)
                {
                    foreach (int[] id in item.PositionInBp)
                    {
                        VirtualBp[id[0], id[1]] = 1;
                    }
                }
            }
            catch(Exception e)
            {
                //No items in bp
            }
            try
            {
                foreach (var consumableItem in ConsumableItems)
                {
                    foreach (int[] id in consumableItem.PositionInBp)
                    {
                        VirtualBp[id[0], id[1]] = 1;
                    }
                }
            }
            catch(Exception e)
            {
                //No items in bp
            }
            int find = 0;
            List<int> widthPosition= new List<int>();
            List<int> heightPosition = new List<int>();
            List<Item> itemsDrop = new List<Item>();
            foreach (var itemUsage in enemy.ItemFromKill)
            {
                itemsDrop.Add(itemUsage);
            }
            foreach (var itemConsumable in enemy.ConsumableItemFromKill)
            {
                itemsDrop.Add(itemConsumable);
            }
            foreach (var item in itemsDrop)
            {               
                int height = 0;
                int widht = 0;
                while (find == itemsDrop.Count)
                {
                    FindItem:
                    for (int j = 0; j < 9; j++)
                    {
                        for (int i = 0; i < 9; i++)
                        {
                            FindWidth:
                            if (VirtualBp[j, i] == 0)
                            {
                                widht++;
                                widthPosition.Add(i);
                                for (int k = 0; k < item.Height; k++)
                                {
                                    if (VirtualBp[j + k, i] == 0)
                                    {
                                        height++;
                                        heightPosition.Add(j);
                                        if (height==item.Height&&widht==item.Width)
                                        {
                                            find++;
                                            foreach (int widthId in widthPosition)
                                            {
                                                foreach (int heightId in heightPosition)
                                                {
                                                    VirtualBp[widthId, heightId] = 1;
                                                }
                                            }
                                            height = 0;
                                            widht = 0;
                                            heightPosition.Clear();
                                            widthPosition.Clear();
                                            goto FindItem;
                                        }
                                        if (height == item.Height)
                                        {
                                            height = 0;
                                            goto FindWidth;
                                        }
                                    }
                                    else
                                    {
                                        height = 0;
                                        widht = 0;
                                        heightPosition.Clear();
                                        widthPosition.Clear();
                                        goto FindWidth;
                                    }
                                }
                            }
                        }
                    }
                    return false;
                }
            }
            return true;
        }      
        public CharacterEntity()
        {
            Lvl = 1;
            Exp = 0;
            Honor = 0;
            HeroClass = Enums.HeroClass.Noone;
            SecondClass = Enums.SubClass.Noone;
            DestinyHero = Enums.Destiny.NoDestiny;
            MaxMoves = 20;
            CurrentMoves = 20;
            HeroCoint = 10;
            Items = new List<ItemEntity>();
            ConsumableItems = new List<ConsumableEntity>();
            Gold = 0;           
        }

    }
}
