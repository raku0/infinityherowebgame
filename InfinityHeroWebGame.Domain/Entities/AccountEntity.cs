﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.ComponentModel.DataAnnotations.Schema;

namespace InfinityHeroWebGame.Domain.Entities
{
    public class AccountEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Index(IsUnique =true)]
        [MinLength(5,ErrorMessage ="Min lenght is 5.")]
        [MaxLength(20,ErrorMessage ="Max lenght is 20.")]
        public string Login { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreateDate { get; set; }
        public bool IsAllowCookie { get; set; }
        [RegularExpression("[a-zA-Z]+", ErrorMessage = "Only letters")]
        public string Name { get; set; }
        [RegularExpression("[a-zA-Z]+",ErrorMessage ="Only letters")]
        public string SurName { get; set; }
        [RegularExpression("[0-9]")]
        public string Phone { get; set; }
        [EmailAddress]
        public string RecoveryEmail { get; set; }
        public bool IsBanned { get; set; }
        public string ReasonOfBan { get; set; }

        public virtual CharacterEntity Character { get; set; }

        public AccountEntity()
        {
            CreateDate = DateTime.Now;
            IsBanned = false;
        }
    }
}