﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using InfinityHeroWebGame.Domain.Constans;
using InfinityHeroWebGame.Domain.Parent;
using InfinityHeroWebGame.Domain.Interface;
using InfinityHeroWebGame.Domain.InfinityHeroDBContex;
using Ninject;
namespace InfinityHeroWebGame.Domain.Entities
{
    public class ItemEntity : Item, IUsageItemCreate
    {
        [Key]
        public int Id { get; set; }
        public Enums.ItemType Type { get; set; }
        public int ILvl { get; set; }
        public Enums.WeaponType WeaponType { get; set; }
        public Enums.DoubleWeaponType DoubleWeaponType { get; set; }
        public bool IsDoubleSlot { get; set; }
        public int? Attack { get; set; }
        public int? MagicAttack { get; set; }
        public int? Defense { get; set; }
        public int? MagicDefense { get; set; }
        public Dictionary<Enums.BonusType, int> BonusValue { get; set; }
        public bool IsEquipment { get; set; }
        public short UpgradePlus { get; set; }
        public Dictionary<Enums.HeroStats,int> RequiredStatToWear { get; set; }


        public ItemEntity()
        {
            BonusValue = new Dictionary<Enums.BonusType, int>();
            RequiredStatToWear = new Dictionary<Enums.HeroStats, int>();
            IsEquipment = false;
            PositionInBp = new List<int[]>();
            UpgradePlus = 0;
        }
        private ItemEntity GetRandomTypeRarityBonus(int lvl)
        {
            ItemEntity itemEntity = new ItemEntity();
            itemEntity.Type = Enums.RandomEnumValue<Enums.ItemType>();
            itemEntity = GetRandomRarityBonus(itemEntity,lvl);                  
            itemEntity.IsDoubleSlot = false;
            if (itemEntity.Type==Enums.ItemType.Weapon)
            {
                itemEntity.WeaponType = Enums.RandomEnumValue<Enums.WeaponType>();
            }
            if (itemEntity.Type==Enums.ItemType.DoubleWeapon)
            {
                itemEntity.DoubleWeaponType = Enums.RandomEnumValue<Enums.DoubleWeaponType>();
                itemEntity.IsDoubleSlot = true;
            }            
            
            return itemEntity;
        }
        //For create specific type of item
        private ItemEntity GetRandomRarityBonus(ItemEntity item,int lvl)
        {
            Random random = new Random();
            item.Rarity = GetRandomRarity();
            item.ILvl = random.Next((int)(lvl * 0.5), (int)(lvl * 1.5));
            int bonus = item.Type == Enums.ItemType.DoubleWeapon ? GetBonusAmount(item.Rarity) * 2 : GetBonusAmount(item.Rarity);
            foreach (Enums.BonusType bonusStat in Enum.GetValues(typeof(Enums.BonusType)))
            {
                if (random.NextDouble() < 5)
                {
                    item.BonusValue.Add(bonusStat, bonus);
                }
            }
            return item;
        }
        private ItemEntity GetProperties(ItemEntity item)
        {
            Random random = new Random();
            int min = 1+(int)(GetBonusAmount(item.Rarity)*0.1);
            int max = 15+ (int)(GetBonusAmount(item.Rarity) * 0.1);
          
            if (item.Type == Enums.ItemType.Weapon)
            {                
                Attack=random.Next(min, max) * (item.ILvl*2);
                MagicAttack=random.Next(min, max) * (item.ILvl*2);
                if (Attack>MagicAttack)
                {
                    RequiredStatToWear.Add(Enums.HeroStats.Strength, + item.ILvl * 4);
                }
                else
                {
                    RequiredStatToWear.Add(Enums.HeroStats.Intelligence, item.ILvl * 4);
                }
            }
            if (item.Type == Enums.ItemType.DoubleWeapon)
            {
                Attack = random.Next(min*2, max*2) * (item.ILvl * 2);
                MagicAttack = random.Next(min*2, max*2) * (item.ILvl * 2);
            }
            else
            {
                Defense = random.Next(min, max) * item.ILvl;
                MagicDefense = random.Next(min, max) * item.ILvl;
            }
            
            return item;
        }
        private ItemEntity GenerateGraphics(ItemEntity item)
        {
            IKernel ninja = new StandardKernel();
            InfinityHeroContext db = ninja.Get<InfinityHeroContext>();
            if (db.GenerateItemRandom())
            {
                if (item.Type == Enums.ItemType.Weapon)
                {
                    item.Name = Names.RandomKeyFromDictionary(Names.GetDictionaryItemsFromFile(item.WeaponType)).ToString();
                    item.ItemGraphics = Names.ValueFromDictionary(item.WeaponType, item.Name);
                }
                if (item.Type == Enums.ItemType.DoubleWeapon)
                {
                    item.Name = Names.RandomKeyFromDictionary(Names.GetDictionaryItemsFromFile(item.DoubleWeaponType)).ToString();
                    item.ItemGraphics = Names.ValueFromDictionary(item.DoubleWeaponType, item.Name);
                }
                else
                {
                    item.Name = Names.RandomKeyFromDictionary(Names.GetDictionaryItemsFromFile(item.Type)).ToString();
                    item.ItemGraphics = Names.ValueFromDictionary(item.Type, item.Name);
                }
                return item;
            }
            else
            {
                if (item.Type == Enums.ItemType.Weapon)
                {
                    item.Name = Names.RandomKeyFromDictionaryByLvl(item.WeaponType, item.ILvl);
                    item.ItemGraphics = Names.ValueFromDictionary(item.WeaponType, item.Name);
                }
                if (item.Type == Enums.ItemType.DoubleWeapon)
                {
                    item.Name = Names.RandomKeyFromDictionaryByLvl(item.DoubleWeaponType, item.ILvl);
                    item.ItemGraphics = Names.ValueFromDictionary(item.DoubleWeaponType, item.Name);
                }
                else
                {
                    item.Name = Names.RandomKeyFromDictionary(Names.GetDictionaryItemsFromFile(item.Type)).ToString();
                    item.ItemGraphics = Names.ValueFromDictionary(item.Type, item.Name);
                }
                return item;
            }
        }
        private ItemEntity GetSizeItem(ItemEntity itemEntity)
        {
            if (itemEntity.Type==Enums.ItemType.Weapon)
            {
                Width = 1;
                Height = 3;
            }
            if (itemEntity.Type == Enums.ItemType.DoubleWeapon)
            {
                Width = 2;
                Height = 3;
            }
            if (itemEntity.Type == Enums.ItemType.Ring || itemEntity.Type == Enums.ItemType.Necklace || itemEntity.Type == Enums.ItemType.Earrings)
            {
                Width = 2;
                Height = 2;
            }
            if (itemEntity.Type==Enums.ItemType.Shield||itemEntity.Type==Enums.ItemType.Armor)
            {
                Width = 3;
                Height = 3;
            }
            if (itemEntity.Type == Enums.ItemType.Boots || itemEntity.Type == Enums.ItemType.Glove || itemEntity.Type == Enums.ItemType.Helmet)
            {
                Width = 2;
                Height = 2;
            }
            return itemEntity;
        }
        
        public ItemEntity CreateRandomItem(Creature creatureToAddItem)
        {
            IKernel ninja = new StandardKernel();
            InfinityHeroContext db = ninja.Get<InfinityHeroContext>();
            ItemEntity item = GetRandomTypeRarityBonus(creatureToAddItem.Lvl);
            item = GenerateGraphics(item);
            item = GetProperties(item);
            item = GetSizeItem(item);
            return item;
        }
        public ItemEntity CreateItem(Creature creatureToAddItem, Enums.ItemType itemType)
        {
            ItemEntity item = new ItemEntity
            {
                Type = itemType
            };
            item = GetRandomRarityBonus(item, creatureToAddItem.Lvl);
            item = GenerateGraphics(item);
            item = GetProperties(item);
            item = GetSizeItem(item);
            return item;
        }
    }
}
