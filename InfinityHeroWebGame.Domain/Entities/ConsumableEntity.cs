﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfinityHeroWebGame.Domain.Parent;
using System.ComponentModel.DataAnnotations;
using InfinityHeroWebGame.Domain.Constans;
using InfinityHeroWebGame.Domain.Interface;

namespace InfinityHeroWebGame.Domain.Entities
{
    public class ConsumableEntity:Item,IConsumableItemCreate
    {
        [Key]
        public int Id { get; set; }
        public Enums.ConsumableType Type { get; set; }
        //effect and time for work
        public Dictionary<Enums.BonusType,int> PotionEffect { get; set; }
        public List<Enums.FoodEffect> FoodEffect { get; set; }
        public Enums.UpgradeEffect UpgradeEffect { get; set; }

        public ConsumableEntity()
        {
            PositionInBp = new List<int[]>();
        }
        private ConsumableEntity GetRandomTypeRarity()
        {
            ConsumableEntity itemEntity = new ConsumableEntity();
            Random random = new Random();
            itemEntity.Rarity = GetRandomRarity();
            itemEntity.Type = Enums.RandomEnumValue<Enums.ConsumableType>();
            int bonus = GetBonusAmount(itemEntity.Rarity);

            return itemEntity;
        }
        public ConsumableEntity CreateRandomConsumableItem(Creature creatureToAddItem)
        {
            ConsumableEntity itemEntity = new ConsumableEntity();
            itemEntity.Rarity = GetRandomRarity();
            Random random = new Random();

            return null;
        }
    }
}
