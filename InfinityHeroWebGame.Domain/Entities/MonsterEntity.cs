﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InfinityHeroWebGame.Domain.Parent;
using System.Threading.Tasks;

namespace InfinityHeroWebGame.Domain.Entities
{
    public class MonsterEntity:Creature
    {
        public bool IsBoss { get; set; }
        public bool BonusDrop { get; set; }

        public MonsterEntity(CharacterEntity hero)
        {
            CreateDrop(hero);
        }
        public void CreateDrop(CharacterEntity hero)
        {
            CreateConsumableDrop(this, hero);
            CreateUsageDrop(this, hero);
        }
    }   
}
