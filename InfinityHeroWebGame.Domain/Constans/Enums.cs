﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfinityHeroWebGame.Domain.Constans
{
    public class Enums
    {
        public enum BonusType
        {
            AddStrenght,
            AddInteligence,
            AddLuck,
            AddAgility,
            AddRambler,
            AddVitality,
            AddHp,
            AddMp,
            AddDefense,
            AddMagicDefense,
            AddAttack,
            AddMagicAttack,
            AddHitPercent,
            AddCritPercent
        }
        public enum ItemType
        {
            Weapon,
            Armor,
            Helmet,
            Glove,
            Ring,
            Necklace,
            Earrings,
            Shield,
            Boots,
            DoubleWeapon,
        }
        public enum WeaponType
        {
            Knife,
            Dagger,
            Sword,
            ShortSword,           
            Axe,
            Mace,
        }
        public enum DoubleWeaponType
        {
            BigAxe,           
            Bow,
            ShortBow,
            LongBow,
            Staff,
            SpellBook,
            Crossbow,
            Wand,
            Rune,
            LongSword,
        }
        public enum HeroStats
        {
            Strength,
            Intelligence,
            Agility,
            Luck,
            Rambler,
            Vitality,
        }
        public enum ConsumableType
        {
            Potion,
            Food,
            Upgrade,
        }
        public enum FoodEffect
        {
            RegenHp,
            RegenMp,
        }
        public enum UpgradeEffect
        {
            UpgradeWeapon,
            UpgradeArmor,
            UpgradeRing,
            UpgradeNeclase,
            UpgradeEarrings,
        }
        public enum HeroClass
        {
            Noone,
        }
        public enum SubClass
        {
            Noone,
        }
        public enum Destiny
        {
            NoDestiny,
        }
        public enum Rarity
        {
            Destroyed,
            Low,
            Normal,
            Rare,
            Unique,
            Epic,
            Legendary           
        }
        public enum ConfigurationAttributes
        {
            NeedLogin,
            ExpRate,
            GameSpeed,
            GoldRate,
            ItemDropGraphicsRandom,
            DoubleDrop,
        };
        public static T RandomEnumValue<T>()
        {
            var v = Enum.GetValues(typeof(T));
            return (T)v.GetValue(new Random().Next(v.Length));
        }
    }
}
