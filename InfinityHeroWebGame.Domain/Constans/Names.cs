﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Web;
using System.Text.RegularExpressions;


namespace InfinityHeroWebGame.Domain.Constans
{
    public class Names
    {
        static string pathGraphics = HttpContext.Current.Server.MapPath("..\\InfinityHeroWebGame\\App_Data\\Items\\");
        //Name of item and path to graphics
        public Dictionary<string,string> KnifeNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.WeaponType.Knife);
            }
        }
        public Dictionary<string, string> DaggerNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.WeaponType.Dagger);
            }
        }
        public Dictionary<string, string> AxeNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.WeaponType.Axe);
            }
        }
        public Dictionary<string, string> SwordNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.WeaponType.Sword);
            }
        }
        public Dictionary<string, string> MaceNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.WeaponType.Mace);
            }
        }
        public Dictionary<string, string> ShortSwordNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.WeaponType.ShortSword);
            }
        }
        public Dictionary<string, string> BigAxeNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.DoubleWeaponType.BigAxe);
            }
        }
        public Dictionary<string, string> BowNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.DoubleWeaponType.Bow);
            }
        }
        public Dictionary<string, string> CrossbowNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.DoubleWeaponType.Crossbow);
            }
        }
        public Dictionary<string, string> LongBowNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.DoubleWeaponType.LongBow);
            }
        }
        public Dictionary<string, string> LongSwordNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.DoubleWeaponType.LongSword);
            }
        }
        public Dictionary<string, string> RuneNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.DoubleWeaponType.Rune);
            }
        }
        public Dictionary<string, string> ShortBowNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.DoubleWeaponType.ShortBow);
            }
        }
        public Dictionary<string, string> SpellBookNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.DoubleWeaponType.SpellBook);
            }
        }
        public Dictionary<string, string> StaffNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.DoubleWeaponType.Staff);
            }
        }
        public Dictionary<string, string> WandNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.DoubleWeaponType.Wand);
            }
        }
        public Dictionary<string, string> ArmorNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.ItemType.Armor);
            }
        }
        public Dictionary<string, string> BootsNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.ItemType.Boots);
            }
        }
        public Dictionary<string, string> EarringsNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.ItemType.Earrings);
            }
        }
    
        public Dictionary<string, string> GloveNames
    {
        get
        {
                return GetDictionaryItemsFromFile(Enums.ItemType.Glove);
            }
    }
    public Dictionary<string, string> HelmetNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.ItemType.Helmet);
            }
        }
        public Dictionary<string, string> NecklaceNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.ItemType.Necklace);
            }
        }
        public Dictionary<string, string> RingNames
        {
            get
            {
                return GetDictionaryItemsFromFile(Enums.ItemType.Ring);
            }
        }
        public Dictionary<string, string> ShieldNames
        {
            get
            {
                
                return GetDictionaryItemsFromFile(Enums.ItemType.Shield);
            }
        }
        public Dictionary<string,string> Monsters
        {
            get
            {
                return null;
            }
        }
        public static string RandomKeyFromDictionaryByLvl(Enum itemType,int lvl)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            Regex name = new Regex(@"[^:]*");
            Regex graphics = new Regex(@"(?<=:)[\w+. -]+");
            Regex iLvlFrom = new Regex(@"(?<=;)[\w+. -]+");
            Regex iLvlTo = new Regex(@"(?<=\/)[\w+. -]+");
            using (FileStream fs = File.Open(pathGraphics + itemType.ToString(), FileMode.OpenOrCreate))
            {
                using (StreamReader sr = new StreamReader(fs))
                {
                    while (sr.Peek() >= 0)
                    {
                        string line = sr.ReadLine();
                        Match key = name.Match(line);
                        Match value = graphics.Match(line);
                        Match iLvlFro = iLvlFrom.Match(line);
                        Match iLvlT = iLvlTo.Match(line);
                        if (key.Success && value.Success && iLvlFro.Success && iLvlT.Success)
                        {
                            if(Convert.ToInt32(iLvlFro.ToString())<lvl&&lvl>Convert.ToInt32(iLvlT.ToString()))
                            dictionary.Add(key.Value, value.Groups[0].Value);
                        }
                    }
                    sr.Close();
                }
            }
            return RandomKeyFromDictionary(dictionary).ToString();
        }
        public static IEnumerable<TKey> RandomKeyFromDictionary<TKey, TValue>(IDictionary<TKey, TValue> dict)
        {
            Random rand = new Random();
            List<TKey> values = Enumerable.ToList(dict.Keys);
            int size = dict.Count;
            while (true)
            {
                yield return values[rand.Next(size)];
            }         
        }
        public static string ValueFromDictionary(Enum dict, string key)
        {
            Dictionary<string, string> dictKey = GetDictionaryItemsFromFile(dict);
            dictKey.TryGetValue(key,out string value);
            return value;
        }
        public static Dictionary<string,string> GetDictionaryItemsFromFile(Enum itemType)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            Regex name = new Regex(@"[^:]*");
            Regex graphics = new Regex(@"(?<=:)[\w+.-]+");
            using (FileStream fs = File.Open(pathGraphics+itemType.ToString(), FileMode.OpenOrCreate))
            {
                using (StreamReader sr = new StreamReader(fs))
                {
                    while (sr.Peek() >= 0)
                    {
                        string line = sr.ReadLine();
                        Match key = name.Match(line);
                        Match value = graphics.Match(line);
                        if(key.Success&&value.Success)
                            dictionary.Add(key.Value, value.Groups[0].Value);                       
                    }
                    sr.Close();
                }
            }
                return dictionary;
        }
    }
}
