﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfinityHeroWebGame.Domain.Entities;
using InfinityHeroWebGame.Domain.InfinityHeroDBContex;
using InfinityHeroWebGame.Domain.Interface;
using Ninject;
using System.Web;
using System.IO;
using System.Data.Entity;
namespace InfinityHeroWebGame.Domain.Parent
{
    public class Creature : IFight
    {
        public string Name { get; set; }
        public int Lvl { get; set; }
        public int MaxHp { get; set; }
        public int CurrentHp { get; set; }
        public int MaxMp { get; set; }
        public int CurrentMp { get; set; }
        public double MinMagickAttack { get; set; }
        public double MaxMagickAttack { get; set; }
        public int Defense { get; set; }
        public int MagicDefense { get; set; }
        public int HitRate { get; set; }
        public int CritRate { get; set; }
        public double MinAttack { get; set; }
        public double MaxAttack { get; set; }
        public ICollection<ItemEntity> ItemFromKill { get; set; }
        public ICollection<ConsumableEntity> ConsumableItemFromKill { get; set; }
        private static IKernel ninja = new StandardKernel();
        private static InfinityHeroContext dbc;   
        public void Attack(Creature Attacker, Creature Defender)
        {
            Random random = new Random();
            if (random.NextDouble() < HitRate)
            {
                int Dmg = random.Next((int)Attacker.MinAttack, (int)Attacker.MaxAttack);
                if (random.NextDouble() < CritRate)
                    Dmg += (int)(Dmg * 0.7);
                decimal protection = Math.Round((decimal)Defense / 100, 1);
                if (protection > 70)
                    protection = 70;
                Dmg -= (int)(((float)protection * Dmg) / 100);
                Defender.CurrentHp -= Dmg;
            }
        }
        public int GetExp(Creature Enemy)
        {
            InfinityHeroContext db = ninja.Get<InfinityHeroContext>();
            Random random = new Random();
            return (int)(((Enemy.Lvl * 1000 * 0.15) + random.Next(Enemy.Lvl * 10, Enemy.Lvl * 200)) * db.GetExpRate());
        }
        public int GetGold(Creature Enemy)
        {
            InfinityHeroContext db = ninja.Get<InfinityHeroContext>();
            Random random = new Random();
            return (int)((Enemy.Lvl * 15) * db.GetGoldRate());
        }
        public ICollection<ItemEntity> CreateUsageDrop(Creature enemy,CharacterEntity hero)
        {
            int j;
            Random random = new Random();
            int UsageChance = 35 + hero.BonusItemFind;
            dbc = ninja.Get<InfinityHeroContext>();
            if (dbc.IsDoubleDrop())
            {
                j = 6;
            }
            else
                j = 3;

            for (int i = 0; i < j; i++)
            {
                if (random.NextDouble() < UsageChance)
                {
                    ItemEntity item = new ItemEntity();
                    ItemFromKill.Add(item.CreateRandomItem(enemy));
                }
            }
            return ItemFromKill;
        }
        public ICollection<ConsumableEntity> CreateConsumableDrop(Creature enemy, CharacterEntity hero)
        {
            int j;
            Random random = new Random();
            int ConsumableChance = 30 + hero.BonusItemFind;
            if (dbc.IsDoubleDrop())
            {
                j = 4;
            }
            else
                j = 2;
            for (int i = 0; i < j; i++)
            {
                if (random.NextDouble() < ConsumableChance)
                {
                    ConsumableEntity item = new ConsumableEntity();
                    ConsumableItemFromKill.Add(item.CreateRandomConsumableItem(enemy));
                }
            }
            return ConsumableItemFromKill;            
        }
        public bool IsDeath(Creature creature)
        {
            return creature.CurrentHp <= 0 ? true : false;
        }
        public bool AddMessageToLog(string message)
        {

            string pathToLog = HttpContext.Current.Server.MapPath("\\App_Data\\PlayerLog\\" + Name);
            if (!File.Exists(pathToLog))
            {
                try
                {
                    FileStream logFile = File.OpenWrite(pathToLog);
                    logFile.Close();
                }
                catch
                {
                    return false;
                }
            }
            try
            {
                byte[] Message = new UTF8Encoding(true).GetBytes(DateTime.Now + ":" + message);
                byte[] newLine = Encoding.ASCII.GetBytes(Environment.NewLine);
                FileStream log = File.Open(pathToLog, FileMode.Append);
                log.Write(Message, 0, Message.Length);
                log.Write(newLine, 0, newLine.Length);
                log.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }


    }
}
