﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfinityHeroWebGame.Domain.Constans;
using InfinityHeroWebGame.Domain.Entities;

namespace InfinityHeroWebGame.Domain.Parent
{
    public class Item
    {
        public string Name { get; set; }
        //Howmuch it takes bp space 
        public int Width { get; set; }
        public int Height { get; set; }
        public string ItemGraphics { get; set; }
        //We need cut the image for ammount of place and then save what space in bp is used for this
        public List<int[]> PositionInBp { get; set; }
        public Enums.Rarity Rarity { get; set; }

        public Enums.Rarity GetRandomRarity()
        {
            Random random = new Random();
            int rarity = random.Next(1, 200);
            Enums.Rarity itemRarity =
                rarity == 1 ? Enums.Rarity.Legendary :
                rarity <= 6 && rarity > 1 ? Enums.Rarity.Epic :
                rarity <= 20 && rarity > 6 ? Enums.Rarity.Unique :
                rarity <= 40 && rarity > 20 ? Enums.Rarity.Rare :
                rarity <= 140 && rarity > 40 ? Enums.Rarity.Normal :
                rarity <= 170 && rarity > 140 ? Enums.Rarity.Low :
                Enums.Rarity.Destroyed;
            return itemRarity;
        }
        public int GetBonusAmount(Enums.Rarity rarity)
        {
            if (rarity == Enums.Rarity.Destroyed)
            {
                return 1;
            }
            if (rarity == Enums.Rarity.Low)
            {
                return 3;
            }
            if (rarity == Enums.Rarity.Normal)
            {
                return 6;
            }
            if (rarity == Enums.Rarity.Rare)
            {
                return 10;
            }
            if (rarity == Enums.Rarity.Unique)
            {
                return 15;
            }
            if (rarity == Enums.Rarity.Epic)
            {
                return 20;
            }
            return 35;
        }
    }
}
