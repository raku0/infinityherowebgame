﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using InfinityHeroWebGame.Domain.Entities;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace InfinityHeroWebGame.Domain.InfinityHeroDBContex
{
    public class InfinityHeroContext : DbContext
    {
        string pathToLog = HttpContext.Current.Server.MapPath("\\App_Data\\ServerLog.txt");
        public InfinityHeroContext() :
            base("InfinityHero")
        {
        }
        public DbSet<ConfigurationEntity> ConfigurationGame { get; set; }
        public DbSet<AccountEntity> GameAccount { get; set; }
        public DbSet<CharacterEntity> GameHero { get; set; }
        public DbSet<ItemEntity> Items { get; set; }
        public DbSet<ConsumableEntity> ConsumableItems { get; set; }

        public bool IsNeedLogin()
        {
            return ConfigurationGame.Single().NeedLogin;
        }
        public double GetExpRate()
        {
            return ConfigurationGame.Single().ExpRate;
        }
        public bool IsDoubleDrop()
        {
            return ConfigurationGame.Single().DoubleDrop;
        }
        public double GetGoldRate()
        {
            return ConfigurationGame.Single().GoldRate;
        }
        public bool GenerateItemRandom()
        {
            return ConfigurationGame.Single().GenerateItemRandom;
        }
        public string HashPassword(string Password)
        {
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
            var pbkdf2 = new Rfc2898DeriveBytes(Password, salt, 2000);
            byte[] hash = pbkdf2.GetBytes(20);
            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);
            return Convert.ToBase64String(hashBytes);
        }
        public bool VerifyUserPassword(string password,string login)
        {
            /* Fetch the stored value */
            string savedPasswordHash = GameAccount.SingleOrDefault(u => u.Login == login).Password;
            /* Extract the bytes */
            byte[] hashBytes = Convert.FromBase64String(savedPasswordHash);
            /* Get the salt */
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            /* Compute the hash on the password the user entered */
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 2000);
            byte[] hash = pbkdf2.GetBytes(20);
            /* Compare the results */
            for (int i = 0; i < 20; i++)
                if (hashBytes[i + 16] != hash[i])
                    return false;
            return true;
        }
        public bool AddMessageToLog(string message)
        {
            try
            {
                byte[] Message = new UTF8Encoding(true).GetBytes(DateTime.Now + ":" + message);
                byte[] newLine = Encoding.ASCII.GetBytes(Environment.NewLine);
                FileStream log = File.Open(pathToLog,FileMode.Append);
                log.Write(Message, 0, Message.Length);
                log.Write(newLine, 0, newLine.Length);
                log.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}